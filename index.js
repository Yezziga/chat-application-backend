const express = require('express')
const http = require('http')
const app = express()
const server = http.createServer(app)

const io = require('socket.io')(server, {
    handlePreflightRequest: (req, res) => {
        const headers = {
            "Access-Control-Allow-Headers": "Content-Type, Authorization, jwt",
            "Access-Control-Allow-Origin": req.headers.origin, //or the specific origin you want to give access to,
            "Access-Control-Allow-Credentials": true
        };
        res.writeHead(200, headers);
        res.end();
    }
});

const PORT = process.env.PORT || 8080;
const cors = require('cors')
const jwt = require('jsonwebtoken');
const fs = require('fs')

app.use(cors())

let clients = [];
let users = [];


io.on('connect', socket => {
    console.log('Client with id ' + socket.id);
    if (socket.handshake.headers["authorization"]) {
        console.log("jwt found: " + socket.handshake.headers["authorization"])
        isAuthorized()
    }
    socket.send({ user: "SERVER", message: 'You have connected to the server!' });
    socket.broadcast.emit('message', { user: "SERVER", message: "someone connected" })
    // console.log(users);

    /**
     * Listens for messages & emits to everyone
     */
    socket.on('message', (data) => {
        socket.emit('message', data)
        socket.broadcast.emit('message', data)
    });

    /**
     * Listens for username (sent on connect)
     * & adds to list and emits to everyone.
     * Sends the list of current online users
     * to the client that connected
     */
    socket.on('username', (data) => {

        // console.log(data + ' username')
        if (users.indexOf(data) < 0) {
            users.push(data)
            socket.emit('addUsers', users)
            clients.push({ username: data, id: socket.id, token: socket.handshake.headers["authorization"] })
            // console.log(users);
            socket.broadcast.emit('addUser', data)
        }

    });

    socket.on('verify', (data)=> {
        isAuthorized(data);
    })

    // socket.on('color', (data) => {
    //     console.log('Color: ' + data);
    //     //find username & emit to everyone
    //     // clientside: save user to each message
    // })

    socket.on('disconnect', (data) => {
        // console.log(data);
        socket.broadcast.emit('message', { user: "SERVER", message: "Someone left ..." })
        let index
        let user
        // find the index of the user with that socketid
        for (let i = 0; i < clients.length; i++) {
            if (clients[i].id == socket.id) {
                user = clients[i].username
                clients.splice(i, 1)
            }
        }

        // remove the user from the list & emit to 
        // all others the name to remove
        for (let i = 0; i < users.length; i++) {
            if (user == users[i]) {
                console.log('removing ' + user);
                socket.broadcast.emit('removeUser', user)
                index = i
            }
        }
        users.splice(index, 1)
        // console.log(users);
    });

});


app.get('/jwt', (req, res) => {
    let privateKey = fs.readFileSync('./private.pem', 'utf8');
    let token = jwt.sign({ "body": "stuff" }, privateKey, { algorithm: 'HS256' });
    // console.log('token: ' + token);
    res.send(token);
})

app.get('/verify', isAuthorized, (req, res) => {
    res.json({ "message": "THIS IS SUPER SECRET, DO NOT SHARE!" })
})

server.listen(PORT,
    () => console.log(`Simple Express app listening on port ${PORT}!`))

    function isAuthorized(token) {
            console.log('token is: ' + token);
            if (token) {
                let privateKey = fs.readFileSync('./private.pem', 'utf8');
                // Here we validate that the JSON Web Token is valid and has been
                // created using the same private pass phrase
                jwt.verify(token, privateKey, { algorithm: "HS256" }, (err, user) => {
        
                    // if there has been an error...
                    if (err) {
                        // shut them out!
                        console.log(err);
                        // res.status(500).json({ error: "Not Authorized" });
                        // throw new Error("Not Authorized");
                    } else {
                        console.log('verifed');
                    }
                    // if the JWT is valid, allow them to hit
                    // the intended endpoint
                });
            } else {
                // No authorization header exists on the incoming
                // request, return not authorized and throw a new error
                // res.status(500).json({ error: "Not Authorized" });
                // throw new Error("Not Authorized");
                console.log('Not authorized');
            }
        }


// function isAuthorized(req, res, next) {
// // function isAuthorized(token) {
//     console.log(token);
//     if (token) {
//         // retrieve the authorization header and parse out the
//         // JWT using the split function
//         let token = req.headers.authorization.split(" ")[1];
//         console.log('blaa');
//         let privateKey = fs.readFileSync('./private.pem', 'utf8');
//         // Here we validate that the JSON Web Token is valid and has been
//         // created using the same private pass phrase
//         jwt.verify(token, privateKey, { algorithm: "HS256" }, (err, user) => {

//             // if there has been an error...
//             if (err) {
//                 // shut them out!
//                 res.status(500).json({ error: "Not Authorized" });
//                 throw new Error("Not Authorized");
//             }
//             // if the JWT is valid, allow them to hit
//             // the intended endpoint
//             return next();
//         });
//     } else {
//         // No authorization header exists on the incoming
//         // request, return not authorized and throw a new error
//         res.status(500).json({ error: "Not Authorized" });
//         throw new Error("Not Authorized");
//     }
// }
